/*
=====================================================================
 File Name      : mainwindow.cpp
 Project        : Conway2
 Author         : S. Weekley
 Version        : 0.0A
 Description 	: This contains code for mainwindow.
=====================================================================
 *
 * Enceladus Confidential
 * __________________
 *
 *  2019 Enceladus Technologies
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Enceladus Technologies and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Enceladus Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Enceladus Technologies.
=====================================================================
 * Comments:
=====================================================================
 * DSW 27MAY19 ->   Initial start to project. Using qt bitmap to create
 *                  the pixel life
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(800,600);
    //this->showFullScreen();
    //QMainWindow::showFullScreen();
    //pixUniverse = new QBitmap(800,600);
    pixUniverse = new QImage(800,600,QImage::Format_RGB32);
    blank = new QImage(800,600,QImage::Format_RGB32);
    //pixUniverse->invertPixels(QImage::InvertRgba);
    ui->statusBar->showMessage("Load Complete");
    //pixUniverse->fill(Qt::white);
    cnStart();
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->setInterval(250);
    lifecycle = 0;

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::cnStart() ///Clears and starts the display
{
    pixUniverse->fill(Qt::white);
    //blank->fill(Qt::white);
    for(int i=0; i<pixUniverse->width(); i++) {
        for(int j=0; j<pixUniverse->height(); j++) {
            //qsrand(time(0));
            //qDebug() << rand();
            //if((rand()%2) == 0) {
            if((init.bounded(0,10) == 0)) {
                //pixUniverse->setPixel(i, j, Qt::black);
                pixUniverse->setPixel(i, j, Qt::black);
            }
        }
    }
    ui->label->setPixmap(QPixmap::fromImage(*pixUniverse));
    ui->statusBar->showMessage("Life initiated.");

}

void MainWindow::update()
{
    /*
     * We need to look to see if there is a living pixel.
     * Then if there is a live pixel, check the sorrounding
     */
//TODO: Apply the conditions to Universe at the same exact moment.
    uniStatus.clear();
    //ui->label->setPixmap(QPixmap::fromImage(*blank));
    //ui->statusBar->showMessage("Life has started. %i Tick", lifecycle);

    for(int i = 0; i < pixUniverse->width(); i++){
        for(int j = 0; j < pixUniverse->height(); j++){
            //qDebug() << pixUniverse->pixelColor(i,j);

            neighbors = liveNeighbors(i, j);
            //qDebug() << i << " , " << j << " " << "Has a living pixel"
            //       << "and " << neighbors << " living neighbors ";

            /*
                 * Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                 */
            if(neighbors < 2) isUnderPopulated(i,j);
            /*
                 * Any live cell with two or three live neighbours lives on to the next generation
                 */
            if((neighbors == 2) or (neighbors == 3)) isLives(i,j);
            /*
                 * Any live cell with more than three live neighbours dies, as if by overpopulation
                 */
            if(neighbors > 3) isOverpopulated(i,j);

            if(liveNeighbors(i,j) == 3) isReproduced(i,j);

        }
    }

    pixUniverse->fill(Qt::white);

    for (int x = 0; x < uniStatus.count(); x++){

        if(uniStatus.at(x).stat == pixStatus::dead ) {
            pixUniverse->setPixelColor(uniStatus.at(x).pixel,Qt::white);
            //qDebug() << "pix at: " << uniStatus.at(x).pixel << " has died.";
        }

        if(uniStatus.at(x).stat == pixStatus::born ){
            pixUniverse->setPixelColor(uniStatus.at(x).pixel,Qt::black);
            //qDebug() << "pix at: " << uniStatus.at(x).pixel << " was born.";
        }

        if(uniStatus.at(x).stat == pixStatus::alive ){
            pixUniverse->setPixelColor(uniStatus.at(x).pixel,Qt::black);
            //qDebug() << "pix at: " << uniStatus.at(x).pixel << " was alive.";
        }

    }

    lifecycle +=1;
    //qDebug() << "# of Living Pixels" << uniStatus.toStdVector().size()/sizeof(QVector);

    ui->label->setPixmap(QPixmap::fromImage(*pixUniverse));

    ui->statusBar->showMessage(QString("Life has started. %1 Tick").arg(lifecycle));
}

void MainWindow::cnClear(){
    pixUniverse->fill(Qt::white);
}

void MainWindow::on_actionStart_triggered()
{
    //timer->start(ui->lineEdit->text().toInt());
    timer->start(2000);
}

void MainWindow::on_actionStop_triggered()
{
    timer->stop();
}

int MainWindow::liveNeighbors(int focus_i, int focus_j)
{
    int living = 0;
    int x = 0;
    int y = 0;
    for (int i = -1 ; i <=1; i++){
        for (int j = -1 ; j <=1; j++){
            x = focus_i + i;
            y = focus_j + j;
            if ((x < 0) || (x > 799) || (y < 0) || (y > 599))
                continue;
            //else if(pixUniverse->pixelColor((x),(y)) != Qt::black)
            else if(pixUniverse->pixelColor((x),(y)) != Qt::white)
                living++;

        }
    }
    return living - 1; //because the focus pixel has to be subtracted from the number

}

void MainWindow::isUnderPopulated(int focus_i, int focus_j) //Any live cell with fewer than two live neighbours dies, as if by underpopulation.
{
    //pixUniverse->setPixelColor();
    //qDebug() << focus_i << " , " << focus_j << " is Underpopulated";
    //pixUniverse->setPixelColor(focus_i,focus_j, Qt::white);
    pix.pixel = QPoint(focus_i,focus_j);
    pix.stat = pixStatus::dead;
    uniStatus.append(pix);
}

void MainWindow::isLives(int focus_i, int focus_j) //Any live cell with two or three live neighbours lives on to the next generation.
{
    //qDebug() << focus_i << " , " << focus_j << " is Living";
    //nothing changes
    //but it needs to be set to born or alive
    pix.pixel = QPoint(focus_i,focus_j);
    pix.stat = pixStatus::alive;
    uniStatus.append(pix);

}

void MainWindow::isOverpopulated(int focus_i, int focus_j) //Any live cell with more than three live neighbours dies, as if by overpopulation.
{
    //qDebug() << focus_i << " , " << focus_j << " is Overpopulated";
    //pixUniverse->setPixelColor(focus_i,focus_j,Qt::white);
    pix.pixel = QPoint(focus_i,focus_j);
    pix.stat = pixStatus::dead;
    uniStatus.append(pix);
}

void MainWindow::isReproduced(int focus_i, int focus_j)
{
    //qDebug() << focus_i << " , " << focus_j << " is Reproduced";
    //pixUniverse->setPixelColor(focus_i,focus_j,Qt::black);
    pix.pixel = QPoint(focus_i,focus_j);
    pix.stat = pixStatus::born;
    uniStatus.append(pix);
}


/*
 *Rules to Conway's Game of Life
 *  Any live cell with fewer than two live neighbours dies, as if by underpopulation.
 *  Any live cell with two or three live neighbours lives on to the next generation.
 *  Any live cell with more than three live neighbours dies, as if by overpopulation.
 *  Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 */



void MainWindow::on_actionExit_triggered()
{
    QApplication::exit();
}
