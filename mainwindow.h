#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPixmap>
#include <QBitmap>
#include <QRandomGenerator>
#include <QDebug>
#include <QImage>
#include <QTimer>

struct pixStatus
{
    QPoint pixel;
    enum status {dead, alive, born};
    status stat;
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //QBitmap *pixUniverse;
    QImage *pixUniverse;
    QImage *blank;
    QTimer *timer;
    int uniStart[800][600];
    int line[800];
    QRandomGenerator init;
    int lifecycle;
    int neighbors;
    QVector <pixStatus> uniStatus;
    pixStatus pix;

    int liveNeighbors(int, int);
    void isUnderPopulated(int, int);
    void isLives(int, int);
    void isOverpopulated(int, int);
    void isReproduced(int, int);

    void cnStart();
    void cnClear();

private slots:
    void on_actionStart_triggered();
    void update();

    void on_actionStop_triggered();

    void on_actionExit_triggered();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H


